import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatPhoneNumber'
})
export class FormatPhoneNumberPipe implements PipeTransform {

  transform(number: string): string {
    return '(' + number.substring(1, 4) + ') ' + number.substring(4, 7) + '-' + number.substring(7);
  }

}
