import { Component, Input, OnInit } from '@angular/core';
import { Organization } from 'src/app/types/organization';

@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.css']
})
export class OrganizationComponent implements OnInit {

  @Input() public organization!: Organization;

  constructor() { }

  ngOnInit(): void {
  }

}
