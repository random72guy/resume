import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-date-range',
  templateUrl: './date-range.component.html',
  styleUrls: ['./date-range.component.css']
})
export class DateRangeComponent implements OnInit {

  @Input() start!: Date;
  @Input() end!: Date;

  public format: string = 'MMM yyyy';

  constructor() { }

  ngOnInit(): void {
  }

}
