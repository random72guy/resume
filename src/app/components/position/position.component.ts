import { Component, Input, OnInit } from '@angular/core';
import { Position } from 'src/app/types/position';

@Component({
  selector: 'app-position',
  templateUrl: './position.component.html',
  styleUrls: ['./position.component.css']
})
export class PositionComponent implements OnInit {

  @Input() public position!: Position;

  constructor() { }

  ngOnInit(): void {
  }

}
