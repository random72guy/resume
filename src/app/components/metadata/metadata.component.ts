import { Component, Input, OnInit } from '@angular/core';
import { Geospatial } from 'src/app/types/geospatial';
import { Temporal } from 'src/app/types/temporal';

@Component({
  selector: 'app-metadata',
  templateUrl: './metadata.component.html',
  styleUrls: ['./metadata.component.css']
})
export class MetadataComponent implements OnInit {

  @Input() elm!: Temporal & Geospatial;

  constructor() { }

  ngOnInit(): void {
  }

}
