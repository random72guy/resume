import { Component, Input, OnInit } from '@angular/core';
import { QuickInfoItem, QuickInfoType } from 'src/app/types/quick-info-item';

@Component({
  selector: 'app-quick-info-item',
  templateUrl: './quick-info-item.component.html',
  styleUrls: ['./quick-info-item.component.css']
})
export class QuickInfoItemComponent implements OnInit {

  public QuickInfoType = QuickInfoType;
  @Input() item!: QuickInfoItem;

  constructor() { }

  ngOnInit(): void {
  }

}
