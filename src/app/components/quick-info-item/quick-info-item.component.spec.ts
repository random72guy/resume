import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuickInfoItemComponent } from './quick-info-item.component';

describe('QuickInfoItemComponent', () => {
  let component: QuickInfoItemComponent;
  let fixture: ComponentFixture<QuickInfoItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuickInfoItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuickInfoItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
