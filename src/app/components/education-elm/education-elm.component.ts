import { Component, Input, OnInit } from '@angular/core';
import { EducationElm } from 'src/app/types/education-elm';
import { Organization } from 'src/app/types/organization';

@Component({
  selector: 'app-education-elm',
  templateUrl: './education-elm.component.html',
  styleUrls: ['./education-elm.component.css']
})
export class EducationElmComponent implements OnInit {

  @Input() public elm!: EducationElm;

  constructor() { }

  ngOnInit(): void {
  }

}
