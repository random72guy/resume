import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EducationElmComponent } from './education-elm.component';

describe('EducationElmComponent', () => {
  let component: EducationElmComponent;
  let fixture: ComponentFixture<EducationElmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EducationElmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EducationElmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
