import { Component } from '@angular/core';
import { achievementsAndHobbies } from 'src/assets/data/achievementsAndHobbies';
import { educationData } from 'src/assets/data/educationData';
import { proficiencyData } from 'src/assets/data/proficiencyData';
import { projectData } from 'src/assets/data/projectData';
import { quickInfoData } from 'src/assets/data/quickInfoData';
import { workExperienceData } from 'src/assets/data/workExperienceData';
import { QuickInfoType } from './types/quick-info-item';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public QuickInfoType = QuickInfoType;
  public quickInfoItems = quickInfoData;
  public education = educationData;
  public workExperience = workExperienceData;
  public technicalProficiencies = proficiencyData;
  public projects = projectData;
  public achievementsAndHobbies = achievementsAndHobbies;

  public lastModified: Date = new Date(document.lastModified);


  public download(): void {
    window.print();
  }

}
