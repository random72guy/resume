export interface Temporal {
  startDate: Date;
  endDate: Date;
}
