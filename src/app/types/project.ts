export interface Project {
  name: string;
  organization: string;
  note: string;
  year: number;
}
