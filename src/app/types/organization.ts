import { Geospatial } from "./geospatial";
import { Position } from "./position";
import { Temporal } from "./temporal";

export interface Organization extends Temporal, Geospatial {
  name: string;
  displaySummarized?: boolean;
  positions: Position[];
  address?: string;
}
