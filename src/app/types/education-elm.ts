import { Temporal } from "./temporal";

export interface EducationElm extends Temporal {
  organization: string;
  degree: string;
  location: string;
}
