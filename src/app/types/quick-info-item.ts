export interface QuickInfoItem {
  icon: string;
  text: string;
  type?: QuickInfoType;
}

export enum QuickInfoType {
  TEXT="TEXT",
  PHONE="PHONE",
  EMAIL="EMAIL",
  URL="URL",
}
