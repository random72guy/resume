import { Temporal } from "./temporal";

export interface Position extends Temporal {
  name: string;
  notes?: string[];
}
