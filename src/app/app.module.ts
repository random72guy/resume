import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PositionComponent } from './components/position/position.component';
import { OrganizationComponent } from './components/organization/organization.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxLodashPipeModule  } from 'ngx-lodash-pipes';
import { EducationElmComponent } from './components/education-elm/education-elm.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { DateRangeComponent } from './components/date-range/date-range.component';
import { MetadataComponent } from './components/metadata/metadata.component';
import { SectionTitleComponent } from './components/section-title/section-title.component';
import { FormatPhoneNumberPipe } from './pipes/format-phone-number.pipe';
import { QuickInfoItemComponent } from './components/quick-info-item/quick-info-item.component';

@NgModule({
  declarations: [
    AppComponent,
    PositionComponent,
    OrganizationComponent,
    EducationElmComponent,
    DateRangeComponent,
    MetadataComponent,
    SectionTitleComponent,
    FormatPhoneNumberPipe,
    QuickInfoItemComponent
  ],
  imports: [
    BrowserModule,
    FlexLayoutModule,
    NgxLodashPipeModule ,
    AppRoutingModule,
    MatIconModule,
    MatButtonModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
