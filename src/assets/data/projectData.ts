import { Project } from "src/app/types/project";

enum Orgs {
  VU = 'VU',
  APL = 'JHU APL',
};
const ONGOING = new Date().getFullYear();

export const projectData: Project[] = [
  {
    name: 'CoA Simulation & Evaluation (SCOPTICS)',
    organization: Orgs.APL,
    year: 2021,
    note: 'Filled leadership need & saved project from cancellation. Set direction, trained team, and met aggressive (1-3 sprint) deliveries.',
  },
  {
    name: 'Test Procedure Catalog (EPIC)',
    organization: Orgs.APL,
    year: ONGOING,
    note: 'Rejuvinated flagging full-stack codebase; accelerated evolution through SE and DevOps. 46 features added. 400 LoC consolidated. 170 tests fixed.'
  },
  {
    name: 'Mobile Mission Status (PSP MOC RealTime)',
    organization: Orgs.APL,
    year: 2020,
    note:
      'Transformed broken prototype into state-of-the-art spacecraft status dashboard for 300+ metrics.'
  },
  {
    name: 'UI and DevOps Guild Founding',
    organization: Orgs.APL,
    year: ONGOING,
    note: '80 disparate practitioners share experience, code, and emergency aid. Reduced project prototyping turnaround by 66%.'
  },
]
