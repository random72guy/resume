import { QuickInfoItem, QuickInfoType } from "src/app/types/quick-info-item";

export const quickInfoData: QuickInfoItem[] = [
  {
    icon: 'work',
    text: 'Software Engineer',
  },
  {
    icon: 'place',
    text: 'Columbia, MD',
  },
  {
    icon: 'phone',
    text: '19143306903',
    type: QuickInfoType.PHONE,
  },
  {
    icon: 'email',
    text: 'chris.r.backofen@gmail.com',
    type: QuickInfoType.EMAIL,
  },
  {
    icon: 'code',
    text: 'GitLab.com/random72guy',
    type: QuickInfoType.URL,
  },
]
