import { EducationElm } from "src/app/types/education-elm";

export const educationData: EducationElm[] = [
  {
    organization: 'Villanova University',
    location: "Villanova, PA",
    startDate: new Date('2010-08-18'),
    endDate: new Date('2014-05-16'),
    degree: "BS Computer Science, Humanities, Honors",
  },
  {
    organization: 'Johns Hopkins University',
    location: 'Laurel, MD',
    startDate: new Date('2017-05-31'),
    endDate: new Date('2020-10-01'),
    degree: 'MS Computer Science',
  }
]
