import { Proficiency } from "src/app/types/proficiency";

export const proficiencyData: Proficiency[] = [{
  name: 'Software Engineering',
  profs: 'Software Patterns, Agile Methods (e.g. Scrum Master, Kanban), Git, SVN.'
},
{
  name: 'User Interface Design',
  profs: 'Data Visualization, Balsamiq, Photoshop, Gimp, Krita.',
},
{
  name: 'Web Development (Front-End)',
  profs: 'HTML5, CSS3, JS/TypeScript, Bootstrap, Material, jQuery, AngularJS, Angular, React, Service Workers, Bower, Node, Grunt.',
},
{
  name: 'Web Development (Back-End)',
  profs: 'PHP, Java, JSP, JUnit, C#, LINQ, Hibernate, Spring, Maven, Gradle. Python. SQL, Firebase, MongoDB.',
},
{
  name: 'DevOps',
  profs: 'Toolchain design, implementation, and maintenance. Docker/Compose, Kubernetes, Ansible, JIRA, Jenkins, Bamboo, GitLab/CI. GUI Test Automation (Eggplant, SikuliX).',
}
];
