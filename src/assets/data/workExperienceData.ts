import { Organization } from "src/app/types/organization";

const ONGOING = new Date();

export const workExperienceData: Organization[] = [
  {
    name: 'Palantir USG',
    startDate: new Date('2022-05-16'),
    endDate: ONGOING,
    location: 'Georgetown, Washington DC',
    address: '1025 Thomas Jefferson St NW, Washington, DC 20007',
    positions: [
      {
        name: 'Forward Deployed Engineer',
        startDate: new Date('2022-05-16'),
        endDate: ONGOING,
        notes: [
          'Infrastructure & product maintenance and capability development for multiple USG customer groups.'
        ]
      }
    ]
  },
  {
    name: 'Booz Allen Hamilton',
    startDate: new Date('2021-11-22'),
    endDate: new Date('2022-04-07'),
    location: 'Arlington, VA (Remote)',
    address: '8283 Greensboro Dr Mining Building, Mc Lean, VA 22102',
    positions: [
      {
        name: 'Senior Full-Stack Dev',
        startDate: new Date('2021-11-22'),
        endDate: new Date('2022-04-07'),
        notes: [
          'Maintained, augmented large, multi-tenant production system.'
        ]
      }
    ]
  },
  {
    name: 'Villanova University',
    location: "Villanova, PA",
    startDate: new Date('2012-05-01'),
    endDate: new Date('2016-01-28'),
    displaySummarized: true,
    address: '800 E. Lancaster Avenue, Villanova, PA 19085',
    positions: [
      {
        name: "Web Application Programmer/Analyst",
        // startDate: new Date('2014-07-28'), // Full time employment start date.
        startDate: new Date('2012-05-01'), // Internship start date.
        endDate: new Date('2016-01-28'),
        notes: [
          'Created popular web/mobile apps for students, faculty.',
          'Presenter, 2014 PA Banner Users Group.',
          'Nine awards for outstanding performance.',
          'Intern 2012-2014; fullhire 2014-2016.',
        ],
      }
    ]
  },
  {
    name: 'Johns Hopkins University Applied Physics Lab',
    location: 'Laurel, MD',
    startDate: new Date('2016-02-08'),
    endDate: new Date('2021-11-12'),
    address: '11100 Johns Hopkins Road, Laurel, Maryland 20723',
    positions: [
      {
        name: 'Large Data Analytics UI Developer',
        startDate: new Date('2016-02-08'),
        endDate: new Date('2019-05-06'),
        notes: [
          'Created and/or maintained 12 configurable, data-driven apps.',
          'Sponsors including FAA, USASOC, US Navy, CBP.',
        ],
      },
      {
        name: 'Space Ground Software Engineer',
        startDate: new Date('2019-05-06'),
        endDate: new Date('2021-11-12'),
        notes: [
          'Increased app stability, agility via refactoring, DevOps, and automation.',
          'Awards for innovation and outstanding performance.',
          'Created 3 apps and 3 DevOps toolchains; improved 3 existing apps.'
        ],
      },
    ],
  },
  {
    name: 'Bluestone Creative',
    displaySummarized: true,
    startDate: new Date('2013-06-01'),
    endDate: new Date('2013-08-31'),
    location: 'Villanova, PA',
    positions: [
      {
        name: 'Web UI Developer',
        startDate: new Date('2013-06-01'),
        endDate: new Date('2013-08-31'),
      }
    ]
  },
  {
    name: 'Internet Office of the Vatican City-State',
    displaySummarized: true,
    startDate: new Date('2013-02-12'),
    endDate: new Date('2013-05-08'),
    location: 'Rome, Italy',
    positions: [
      {
        name: 'Web and Mobile UI Intern',
        startDate: new Date('2013-02-12'),
        endDate: new Date('2013-05-08'),
      }
    ]
  },
]
